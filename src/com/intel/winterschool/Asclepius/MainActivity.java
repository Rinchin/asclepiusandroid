//public class MainActivity extends Activity {
//    private String[] categoriesListValues;
//    private ListView categoriesList;
//    private ListView selectedSymptoms;
//    private SQL_adapter myDataBaseAdapter;
//    private Button showDiseases;
//    private Cursor cursor;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        Log.d("ASKL","++++");
//
//        showDiseases = (Button) findViewById(R.id.b_searchDisease);
//        showDiseases.setOnClickListener( new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent();
//
//                startActivity( new Intent());
//            }
//        });
////
////        myDataBaseAdapter = new SQL_adapter(this);
////        categoriesList = (ListView) findViewById(R.id.mainList);
////        selectedSymptoms = (ListView) findViewById(R.id.l_left_drawer);
////
////        categoriesList.setAdapter( new SimpleCursorAdapter(this,
////                R.layout.list_item, myDataBaseAdapter.getCategoriesDisease(),
////                null, null) );
////        setUpContent();
//
//        selectedSymptoms.setOnItemClickListener(new DrawerItemClickListener());
package com.intel.winterschool.Asclepius;

import android.app.*;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.*;
import android.widget.*;

import java.util.*;

/**
 * This example illustrates a common usage of the DrawerLayout widget
 * in the Android support library.
 * <p/>
 * <p>When a navigation (left) drawer is present, the host activity should detect presses of
 * the action bar's Up affordance as a signal to open and close the navigation drawer. The
 * ActionBarDrawerToggle facilitates this behavior.
 * Items within the drawer should fall into one of two categories:</p>
 * <p/>
 * <ul>
 * <li><strong>View switches</strong>. A view switch follows the same basic policies as
 * list or tab navigation in that a view switch does not create navigation history.
 * This pattern should only be used at the root activity of a task, leaving some form
 * of Up navigation active for activities further down the navigation hierarchy.</li>
 * <li><strong>Selective Up</strong>. The drawer allows the user to choose an alternate
 * parent for Up navigation. This allows a user to jump across an app's navigation
 * hierarchy at will. The application should treat this as it treats Up navigation from
 * a different task, replacing the current task stack using TaskStackBuilder or similar.
 * This is the only form of navigation drawer that should be used outside of the root
 * activity of a task.</li>
 * </ul>
 * <p/>
 * <p>Right side drawers should be used for actions, not navigation. This follows the pattern
 * established by the Action Bar that navigation should be to the left and actions to the right.
 * An action should be an operation performed on the current contents of the window,
 * for example enabling or disabling a data overlay on top of the current content.</p>
 */
public class MainActivity extends Activity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ListView mCategoriesList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    private FragmentTransaction fTrans = null;
    private boolean visibleMainFragment = true;

    Fragment fragment;
    private ArrayList<String> mStr;
    private ArrayAdapter<String> categories_adapter; private ArrayAdapter<String> selected_symptoms_adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);


        mTitle = getTitle();
        mDrawerTitle = "Выбранные:";//STUB!!!!!!!!!

        mStr = new ArrayList<String>(100);
        mStr.add("first"); mStr.add("second"); mStr.add("third");

        selected_symptoms_adapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, mStr);
               categories_adapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, mStr);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mCategoriesList = (ListView) findViewById(R.id.l_symptoms_categories);

//STUB!!!!!!!!!!!!
        mCategoriesList.setAdapter(categories_adapter);
        mCategoriesList.setOnItemClickListener(new CategorySymptomsListener());
        mDrawerList.setAdapter(selected_symptoms_adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // set a custom shadow that overlays the activity_main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(getString(R.string.drawer_close));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(getString(R.string.drawer_open));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        menu.findItem(R.id.action_commit_s_l).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        menu.findItem(R.id.action_commit_s_l).setVisible(drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
                // catch event that there's no activity to handle intent
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener

    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Log.d("WWW", parent + "_" + position + "_" + (view == null));
            mStr.remove(position);
            selected_symptoms_adapter.notifyDataSetChanged();
        }
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public class FragmentSymptoms extends Fragment {
        TextView category_title = null;
        String string = null;
        public FragmentSymptoms(String string){
            this.string = string;
        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_symptoms, null);
            category_title = (TextView) v.findViewById(R.id.category_title);
            category_title.setText(string);
            return v;
        }

        public void onStop() {
            Log.d("ASKL","eee");//ПЕРЕДАЧА СПИСКА ВЫБРАННЫХ СИМПТОМОВ
            super.onStop();
        }
    }

    private class CategorySymptomsListener implements AdapterView.OnItemClickListener

    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            fTrans = getFragmentManager().beginTransaction();
            fragment = new FragmentSymptoms((String) ((TextView) view).getText());

            fTrans.replace(R.id.content_frame, fragment);
            fTrans.addToBackStack(null);
            fTrans.commit();
        }
    }


    public void oooooo(){}

    public void oiiiii(){}
    //11111111111111111111111111111111111111111111
}



