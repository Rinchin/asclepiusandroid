package com.intel.winterschool.Asclepius;

import android.app.ListActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.widget.ArrayAdapter;

public class Symptoms extends Activity implements OnClickListener{
    final String LOG_TAG = "myLogs";

    ListView lvSymp;
    String[] LV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.symptoms);
        Button button1 = (Button)findViewById(R.id.btnsymp);
        button1.setOnClickListener(this);
//////////

        lvSymp = (ListView) findViewById(R.id.list);

        lvSymp.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.LV,android.R.layout.simple_list_item_multiple_choice);
        lvSymp.setAdapter(adapter);

        Button btnChecked = (Button) findViewById(R.id.btnsymp);
        btnChecked.setOnClickListener(this);


        LV = getResources().getStringArray(R.array.LV);
    }

    public void onClick(View v) {
        /*Log.d(LOG_TAG, "checked: ");
        SparseBooleanArray sbArray = lvSymp.getCheckedItemPositions();
        for (int i = 0; i < sbArray.size(); i++) {
            int key = sbArray.keyAt(i);
            if (sbArray.get(key))
                Log.d(LOG_TAG, LV[key]);
        }*/

        switch (v.getId()) {
            case R.id.btnsymp:
                Intent intent = new Intent(Symptoms.this, MainActivity.class);
                startActivity(intent);
                break;

        }

        }



        /*for(int i = 1; i < 6; i++) {
            CheckBox cb = new CheckBox(this);
            cb.setText("симптом " + i);
            cb.setId(i+10);
            ll.addView(cb);
        }
        /*Button b = new Button(this);
        b.setText("Готово!");

        //b.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        b.setId(MY_BUTTON);
        b.setOnClickListener(this);
        ll.addView(b);*/


       /* ToggleButton tb = new ToggleButton(this);
        tb.setTextOn("maybe - ON");
        tb.setTextOff("maybe - OFF");
        tb.setChecked(true);
        tb.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        ll.addView(tb);*/


    }
