package com.intel.winterschool.Asclepius;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

/**
 * Created by Arlis on 03.02.14.
 */
public class SQL_adapter {
    private int dbVer = 1;
    private MyDBHelper myDBHelper;
    private SQLiteDatabase sqLiteDatabase;

    public SQL_adapter( Context context){
        myDBHelper = new MyDBHelper( context, dbVer);
    }

    public void open() throws SQLException {
        sqLiteDatabase = myDBHelper.getReadableDatabase();
    }

    public void close(){
        sqLiteDatabase.close();
    }

    public Cursor getExamination(){
        Cursor cursor = null;
        //----------------------
        return cursor;
    }

    //получить указатель на спискок категорий болезней
    public Cursor getCategoriesDisease(){
        Cursor cursor = null;
        //TO DO
        return cursor;
    }
    //получить указатель на список болезней
    public Cursor getDisease(){
        Cursor cursor = null;// = sqLiteDatabase.query();
        //TO DO
        return cursor;
    }
    //получить указатель на список болезней, удовлетворяющих списку симптомов
    public Cursor getListDisease(){
        Cursor cursor = null;
        //TO DO
        return cursor;
    }

    //получить указатель на список симптомов
    public Cursor getSymptoms(){
        Cursor cursor = null;
        //TO DO
        return cursor;
    }
    //получить указатель на список категорий симптомов
    public Cursor getCategorySymptoms(){
        Cursor cursor = null;
        //TO DO
        return cursor;
    }


    private void update() {

    }

    //вложеный класс
    class MyDBHelper extends SQLiteOpenHelper {
        final String CREATE_TABLES = "!!!!!!!!!!!!!!!!!!!!";
        private static final String DATABASE_NAME = "AsclepiusBD.db";
        Context mContext;

        public MyDBHelper(Context context, int DATABASE_VERSION){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            mContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLES);
            update();
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //проверяете какая версия сейчас и делаете апдейт
//            db.execSQL("DROP TABLE IF EXISTS tableName");
//            onCreate(db);
        }
    }
}
