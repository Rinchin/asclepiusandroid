package com.intel.winterschool.Asclepius;

/**
 * Created by Craig on 04.02.14.
 */
 import android.app.Activity;
 import android.content.Intent;
 import android.content.SharedPreferences;
 import android.content.SharedPreferences.Editor;
 import android.os.Bundle;
 import android.preference.PreferenceManager;
 import android.view.Menu;
 import android.view.MenuItem;
 import android.view.View;
 import android.view.View.OnClickListener;
 import android.widget.Button;
 import android.widget.EditText;
 import android.widget.Toast;
 import android.app.Activity;
 import android.content.SharedPreferences;
 import android.os.Bundle;
 import android.preference.Preference;
 import android.preference.PreferenceActivity;
 import android.preference.Preference.OnPreferenceClickListener;


public class Preferences extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final int IDM_PREF = 101;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        /*SharedPreferences.OnSharedPreferenceChangeListener prefListener = new SharedPreferences().OnSharedPreferenceChangeListener();*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu )
    {
        menu.add(0,  IDM_PREF, 0, "Настройки");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Intent intent = new Intent();
        switch (item.getItemId())
        {
            case IDM_PREF:
                intent.setClass(this, MainActivity.class);
                startActivity(intent);
                break;
        }
        return false;
    }



    public void onSharedPreferenceChanged(SharedPreferences Spref,String key)
        {

        }

}
