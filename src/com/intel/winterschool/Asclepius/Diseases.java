package com.intel.winterschool.Asclepius;

/**
 * Created by Craig on 03.02.14.
 */
            import android.app.ListActivity;
            import android.os.Bundle;
            import android.app.Activity;
            import android.content.Intent;
            import android.util.Log;
            import android.view.Gravity;
            import android.view.View;
            import android.view.View.OnClickListener;
            import android.widget.*;
            import android.widget.LinearLayout.LayoutParams;
            import android.widget.AdapterView.OnItemSelectedListener;

public class Diseases extends Activity implements OnClickListener {

    ListView lvDis;
    String[] LV;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diseases);
        Button button1 = (Button)findViewById(R.id.btndis);
        button1.setOnClickListener(this);
        lvDis = (ListView) findViewById(R.id.list);
        lvDis.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.LV,android.R.layout.simple_list_item_multiple_choice);
        lvDis.setAdapter(adapter);

        Button btnChecked = (Button) findViewById(R.id.btndis);
        btnChecked.setOnClickListener(this);


        LV = getResources().getStringArray(R.array.LV);
    }
    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.btndis:
                Intent intent = new Intent(Diseases.this, Examination.class);
                startActivity(intent);
                break;
        }
    }
}

