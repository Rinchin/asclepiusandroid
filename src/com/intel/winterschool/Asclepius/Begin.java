package com.intel.winterschool.Asclepius;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;


public class Begin extends Activity implements OnClickListener {




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.begin);
        Button button1 = (Button)findViewById(R.id.buttohelp);
        button1.setOnClickListener(this);
        Button button2 = (Button)findViewById(R.id.buttondict);
        button2.setOnClickListener(this);

    }
    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.buttohelp:
                Intent intent = new Intent(Begin.this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.buttondict:
                Intent intent1 = new Intent(Begin.this, MainActivity.class);
                startActivity(intent1);
                break;
        }
    }
}