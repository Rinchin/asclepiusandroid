//package updater;
//
//import updater.tables.Table;
//
//import java.io.IOException;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//import java.net.Socket;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.LinkedList;
//import java.util.Queue;
//import java.util.Set;
//
//public class UpdateReceiver implements Runnable
//{
//    private String username;
//    private String password;
//    private int revision;
//
//    private Queue<String> queries;
//
//    UpdateReceiver(String username, String password, int revision)
//    {
//        this.username = username;
//        this.password = password;
//
//        this.revision = revision;
//    }
//
//    @Override
//    public void run()
//    {
//        try
//        {
//            Socket socket = new Socket("127.0.0.1", UpdateServer.PORT);
//            try
//            {
//                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
//                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
//
//                Connector connector = new Connector("receiver");
//
//                ResultSet set = connector.executeQuery("SELECT MAX(id) AS revision FROM revisions");
//
//                try
//                {
//                    revision = set.getInt("revision");
//                }
//                catch (SQLException e)
//                {
//                    revision = -1;
//                }
//
//                outputStream.writeObject(username);
//                outputStream.writeObject(password);
//                outputStream.writeInt(revision);
//                outputStream.flush();
//
//                Queue<String> queue = new LinkedList<>();
//
//                for (Table.Registry table : Table.Registry.values())
//                {
//                    String tablename = table.getName();
//
//                    int deletedCount = inputStream.readInt();
//
//                    StringBuilder query = new StringBuilder("DELETE FROM ");
//
//                    query.append(tablename).append(" WHERE id IN (-1");
//
//                    for (int i = 0; i < deletedCount; i++)
//                    {
//                        query.append(", ").append(inputStream.readLong());
//                    }
//
//                    if (deletedCount != 0)
//                    {
//                        query.setLength(query.length() - 2);
//                    }
//
//                    query.append(')');
//
//                    System.out.println(query);
//                    //connector.executeUpdate(String.valueOf(query));
//                    queue.add(String.valueOf(query));
//
//                    int updatedCount = inputStream.readInt();
//
//                    query.setLength(0);
//                    query.append("UPDATE TABLE ").append(tablename).append(' ');
//
//                    int prefixLength = query.length();
//
//                    for (int i = 0; i < updatedCount; i++)
//                    {
//                        Table tableExample = (Table)inputStream.readObject();
//
//                        query.setLength(prefixLength);
//
//                        Set<String> fieldNames = tableExample.getFieldNames();
//
//                        String id = tableExample.getField("id");
//                        fieldNames.remove("id");
//
//                        for (String field : fieldNames)
//                        {
//                            query.append("SET ").
//                                    append(field).
//                                    append("=").
//                                    append(tableExample.getField(field)).
//                                    append(", ");
//                        }
//
//                        query.setLength(query.length() - 2);
//
//                        query.append(" WHERE id = ").append(id);
//
//                        System.out.println(query);
//                        //connector.executeUpdate(String.valueOf(query));
//
//                        queue.add(String.valueOf(query));
//                    }
//
//                    int createdCount = inputStream.readInt();
//
//                    query.setLength(0);
//                    query.append("INSERT INTO ").append(tablename).append(" VALUES (");
//
//                    prefixLength = query.length();
//
//                    for (int i = 0; i < createdCount; i++)
//                    {
//                        Table tableExample = (Table)inputStream.readObject();
//
//                        query.setLength(prefixLength);
//
//                        Set<String> fields = tableExample.getFieldNames();
//
//                        for (String field : fields)
//                        {
//                            query.append(tableExample.getField(field)).
//                                    append(", ");
//                        }
//
//                        query.setLength(query.length() - 2);
//
//                        query.append(')');
//
//                        System.out.println(query);
//                        //connector.executeUpdate(String.valueOf(query));
//                        queue.add(String.valueOf(query));
//                    }
//                }
//
//                revision = inputStream.readInt();
//
//                outputStream.writeInt(156);
//                outputStream.flush();
//
//                queries = queue;
//            }
//            catch (ClassNotFoundException | SQLException e)
//            {
//                e.printStackTrace();
//            }
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    public synchronized Queue<String> getQueries()
//    {
//        return queries;
//    }
//
//    public synchronized int getNewRevision()
//    {
//        return revision;
//    }
//
//    public static void main(String[] args)
//    {
//        new UpdateReceiver("justin", "kote", 0).run();
//    }
//}